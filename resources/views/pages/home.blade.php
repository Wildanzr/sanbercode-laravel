@extends('admin/template')

@section('page-title')
  <h1>Dashboard</h1>
@endsection

@section('title')
<header>
  <h1>SanberBook</h1>
  <h2>Sosial Media Developer Santai Berkualitas</h2>
  <p>Belajar dan berbagi agar hidup ini semakian santai berkualitas</p>
</header>
@endsection
    

@section('content')
<main>
  <div class="benefit">
    <h3>Benefit Join di SanberBook</h3>
    <ul>
      <li>Mendapatkan motivasi dari sesama developer</li>
      <li>Sharing knowledge dari para mastah Sanber</li>
      <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
  </div>
  <div class="join">
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
      <li>Mengunjungi Website ini</li>
      <li>Mendaftar di <a href="./register">Form Sign Up</a></li>
      <li>Selesai!</li>
    </ol>
  </div>
</main>
@endsection