@extends('admin/template')

@section('page-title')
<h1>Create An Account</h1>
@endsection


@section('title')
<h1>Buat Account Baru</h1>
@endsection


@section('content')
<main>
  <h2>Sign Up Form</h2>
  <form action="./welcome" method="POST">
    @csrf
    <div class="personName">
      <p>First Name:</p>
      <input type="text" id="fName" name="Fname" required />
      <br />
      <p>Last Name:</p>
      <input type="text" id="lName" name="Lname" required />
      <br />
    </div>

    <div class="personGender">
      <p>Gender:</p>
      <input type="radio" id="male" value="Male" name="gender" required />
      <label for="male">Male</label>
      <br />
      <input type="radio" id="female" value="Female" name="gender" />
      <label for="female">Female</label>
      <br />
      <input type="radio" id="other" value="Other" name="gender" />
      <label for="other">Other</label>
      <br />
    </div>

    <div class="personNationality">
      <p>Nationality:</p>
      <select id="nationality" name="nationality" required>
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Thailand">Thailand</option>
        <option value="Filipina">Filipina</option>
        <option value="Kamboja">Kamboja</option>
        <option value="Vietnam">Vietnam</option>
        <option value="Brunei">Brunei</option>
        <option value="Laos">Laos</option>
        <option value="Myanmar">Myanmar</option>
        <option value="Timor Leste">Timor Leste</option>
      </select>
      <br />
    </div>

    <div class="personLanguage">
      <p>Language Spoken:</p>
      <input
        type="checkbox"
        name="indonesia"
        id="indonesia"
        value="Bahasa Indonesia"
      />
      <label for="indonesia">Bahasa Indonesia</label>
      <br />
      <input type="checkbox" name="english" id="english" value="English" />
      <label for="english">English</label>
      <br />
      <input type="checkbox" name="other" id="other" value="Other" />
      <label for="other">Other</label>
      <br />
    </div>

    <div class="personBio">
      <p>Bio:</p>
      <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
      <br />
    </div>

    <input type="submit" value="Sign Up" />
  </form>
</main>
@endsection
