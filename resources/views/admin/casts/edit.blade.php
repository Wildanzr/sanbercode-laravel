@extends('admin/template')

@section('page-title')
<h1>Cast</h1>
@endsection


@section('title')
<h3>Cast Edit</h3>
@endsection

@section('content')
<form id="myForm" action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" value="{{$cast->name}}" id="name" placeholder="cast name">
  </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Age</label>
    <input type="number" class="form-control" name="age"  value="{{$cast->age}}" id="age" placeholder="cast age (number)">
  </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" id="bio" rows="5" cols="30" placeholder="My name is ....... Iam a professional actor .........">{{$cast->bio}}</textarea>
  </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
