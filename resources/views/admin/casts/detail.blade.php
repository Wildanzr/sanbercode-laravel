@extends('admin/template')

@section('page-title')
<h1>Cast</h1>
@endsection


@section('title')
<h3>Cast Detail</h3>
@endsection


@section('content')
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">{{$cast->name}}</h5>
    <br>
    <h6 class="card-subtitle mb-2 text-muted">Age : {{$cast->age}}</h6>
    <br>
    <p class="card-text">{{$cast->bio}}</p>
  </div>
</div>

<a href="/cast" class="btn btn-secondary mt-3">Back</a>
@endsection
