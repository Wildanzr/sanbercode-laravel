@extends('admin/template')

@section('page-title')
<h1>Cast</h1>
@endsection


@section('title')
<h1>Active Cast List</h1>
@endsection


@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Add Cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Name</th>
      <th scope="col">Age</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($casts as $key => $item)
      <tr>
        <td>{{$key+1}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->age}}</td>
        <td>{{$item->bio}}</td>
        <td>
          <form action="/cast/{{$key+1}}" method="POST">
            @csrf
            @method('DELETE')
            <a href="/cast/{{$item->id}}" class="btn btn-primary ml-2">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning ml-2">Edit</a>
            <input type=submit class="btn btn-danger ml-2" value="Delete">
          </form> 
        </td>
      </tr>
    @empty
    <tr>
      <td colspan="5">No Data</td>
    </tr>
    @endforelse
    
  </tbody>
</table>
@endsection
