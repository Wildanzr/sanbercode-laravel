@extends('admin/template')

@section('page-title')
<h1>Cast</h1>
@endsection


@section('title')
<h3>Cast Form</h3>
@endsection


@section('content')
<form id="myForm" action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="cast name">
  </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Age</label>
    <input type="number" class="form-control" name="age" id="age" placeholder="cast age (number)">
  </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" id="bio" rows="5" cols="30" placeholder="My name is ....... Iam a professional actor ........."></textarea>
  </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Add Cast</button>
</form>
@endsection
