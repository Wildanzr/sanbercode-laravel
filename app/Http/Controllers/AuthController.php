<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formRegister() {
        return view ('./pages/register');
    }

    public function userRegister(Request $request) {
        $fName = $request->input('Fname');
        $lName = $request->input('Lname');

        return view ('./pages/welcome', compact('fName', 'lName'));
    }
}
