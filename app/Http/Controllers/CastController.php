<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('/admin/casts/index', ['casts' => $casts]);
    }

    public function create() 
    {
        return view('/admin/casts/create');
    }

    public function store(Request $request) 
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric|min:1|max:150',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Name should not empty',
            'age.required'  => 'Age should not empty',
            'age.required'  => 'Bio should not empty',
            'age.min'  => 'Age should be between 1 and 150',
            'age.max'  => 'Age should be between 1 and 150',
        ]
        );

        DB::table('casts')->insert([
            'name' => $request->name,
            'age' => $request->age,
            'bio' => $request->bio,
        ]);

        return redirect('/cast/create');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('/admin/casts/detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        
        return view('/admin/casts/edit', ['cast' => $cast]);
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required|numeric|min:1|max:150',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Name should not empty',
            'age.required'  => 'Age should not empty',
            'age.required'  => 'Bio should not empty',
            'age.min'  => 'Age should be between 1 and 150',
            'age.max'  => 'Age should be between 1 and 150',
        ]
        );

        DB::table('casts')->where('id', $id)->update([
            'name' => $request->name,
            'age' => $request->age,
            'bio' => $request->bio,
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
